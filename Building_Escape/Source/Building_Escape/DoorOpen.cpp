// Fill out your copyright notice in the Description page of Project Settings.

#include "DoorOpen.h"

// Sets default values for this component's properties
UDoorOpen::UDoorOpen()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UDoorOpen::BeginPlay()
{
	Super::BeginPlay();

	// ...
    ActorThatOpens = GetWorld()->GetFirstPlayerController()->GetPawn();
	
}


// Called every frame
void UDoorOpen::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
    if (PressurePlate and PressurePlate->IsOverlappingActor(ActorThatOpens))
    {
        OpenDoor();
        DoorLastOpenTime = GetWorld()->GetTimeSeconds();
    }
    
    if (GetWorld()->GetTimeSeconds() - DoorLastOpenTime > DoorCloseDelay ) {
        CloseDoor();
    }
}

void UDoorOpen::OpenDoor() {
    
    // find the owning Actor
    AActor* Owner = GetOwner();
    
    // create a rotator
    FRotator NewRotation = FRotator(0.0f, 180.0f, 0.0f);
    
    // Set the door rotation
    Owner->SetActorRotation(NewRotation);
}

void UDoorOpen::CloseDoor() {
    
    // find the owning Actor
    AActor* Owner = GetOwner();
    
    // create a rotator
    FRotator NewRotation = FRotator(0.0f, 90.0f, 0.0f);
    
    // Set the door rotation
    Owner->SetActorRotation(NewRotation);
}

