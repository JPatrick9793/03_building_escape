// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Runtime/Engine/Public/CollisionQueryParams.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Runtime/Engine/Classes/GameFramework/PlayerController.h"
#include "Runtime/Engine/Classes/Components/PrimitiveComponent.h"

#include "Components/ActorComponent.h"
#include "Components/InputComponent.h"
#include "PhysicsEngine/PhysicsHandleComponent.h"
#include "DrawDebugHelpers.h"
#include "Grabber.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BUILDING_ESCAPE_API UGrabber : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UGrabber();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

private:
    // what is the players reach distance for grabbing?
	float Reach = 300.f;
    
    UPhysicsHandleComponent* PhysicsHandle = nullptr;

    // input component is what bind actions to keys
    UInputComponent* InputComponent = nullptr;
    
    // simplified function which draws a debug line
	void myDrawLine(FVector startPoint, FVector endPoint, FColor rgb);

	// function to set all starting variables
	void SetStartingVariables();
    // position within the world that the player started at
	FVector StartingPosition;
    // players starting rotation (not really needed)
	FRotator StartingRotation;

	// Ray-cast and grab whats in reach
	void Grab();

	// function called when grab button is released
	void Release();

	void FindPhysicsHandleComponent();

	void SetupInputComponent();

	FHitResult GetFirstPhysicsBodyInReach();

    FVector GetEndOfReach() const;

	FVector GetPlayerLocation() const;
};
