// Fill out your copyright notice in the Description page of Project Settings.

#include "Grabber.h"
//#include <NiagaraEditor/Public/INiagaraCompiler.h>

// Sets default values for this component's properties
UGrabber::UGrabber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();

	/// ...
	UE_LOG(LogTemp, Warning, TEXT("Grabber reporting for duty!"));

	/// set starting instance variables
    SetStartingVariables();

    /// look for physics handle component
    FindPhysicsHandleComponent();

    /// look for input component to bind actions
    SetupInputComponent();

}

// looks for input components and binds actions appropriately
void UGrabber::SetupInputComponent()
{
    InputComponent = GetOwner()->FindComponentByClass<UInputComponent>();

    /// if input component exists, bind actions
    if (InputComponent)
	{
		InputComponent->BindAction("Grab", IE_Pressed, this, &UGrabber::Grab);
		InputComponent->BindAction("Grab", IE_Released, this, &UGrabber::Release);
	}
	/// else, log that no input component was found
	else
	{
		UE_LOG(LogTemp, Error, TEXT("InputComponent NOT Found!"));
	}
}

// function to set starting instance variables defined in header file
void UGrabber::SetStartingVariables()
{
    GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
            StartingPosition,
            StartingRotation
	);
}

// method which looks for a PhysicsHandleComponent
void UGrabber::FindPhysicsHandleComponent()
{

    /// attempt to get the physics handle from Owner class
    PhysicsHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();

    /// print the name
    if (PhysicsHandle)
	{
		UE_LOG(LogTemp, Warning, TEXT("Physics Handle Found!"));
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Physics Handle NOT Found!"));
	}
}

// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

    // if physics handle is attached
	if (PhysicsHandle->GrabbedComponent) {
		// move object that we are holding
        FVector endOfRay = GetEndOfReach();
		PhysicsHandle->SetTargetLocation(endOfRay);
	}


}

// returns an FVector representing the end of "reach"
FVector UGrabber::GetEndOfReach() const
{
	// initialize two variables for location and rotation
    FVector PlayerViewPointLocation;
    FRotator PlayerViewPointRotation;

    // update those variables with function
	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
			PlayerViewPointLocation,
			PlayerViewPointRotation
	);

    // setup query parameters
    FCollisionQueryParams traceParameters(FName(TEXT("")), false, GetOwner());

    // this is the END of the vector representing the raycast
    FVector endOfRay = PlayerViewPointLocation + Reach * PlayerViewPointRotation.Vector();

    // return the end of reach "ray"
    return endOfRay;
}

// returns FVector for players location
FVector UGrabber::GetPlayerLocation() const
{
	FVector location = GetWorld()->GetFirstPlayerController()->GetFocalLocation();
	return location;
}

// Get player to view this tick
FHitResult UGrabber::GetFirstPhysicsBodyInReach()
{

    // setup query parameters
    FCollisionQueryParams traceParameters(FName(TEXT("")), false, GetOwner());

	// resulting hit
	FHitResult hit;

	// do that actual line-tracing
	GetWorld()->LineTraceSingleByObjectType(hit, GetPlayerLocation(), GetEndOfReach(),
			FCollisionObjectQueryParams(ECC_PhysicsBody), traceParameters
			);

	// get actor which was hit
	AActor* actorHit = hit.GetActor();

	// log the actors name to the console
	if (actorHit)
		UE_LOG(LogTemp, Warning, TEXT("Grabber has hit %s"), *(actorHit->GetName()));

	// return the hit object
	return hit;
}

// method to draw a simple debug line
void UGrabber::myDrawLine(FVector startPoint, FVector endPoint, FColor rgb)
{
	DrawDebugLine(
		GetWorld(),
		startPoint,
		endPoint,
		rgb,
		false,
		0.f,
		0.f,
		10.f
	);
}

void UGrabber::Grab()
{
	UE_LOG(LogTemp, Warning, TEXT("Grab pressed!"));

	// Line-trace and get first physics body in reach
	FHitResult hit = GetFirstPhysicsBodyInReach();

	UPrimitiveComponent * ComponentToGrab = hit.GetComponent();

	auto ActorHit = hit.GetActor();

	// do something with it
	if (ActorHit != nullptr) {

	    PhysicsHandle->GrabComponentAtLocationWithRotation(
	            ComponentToGrab,
	            NAME_None,
	            ComponentToGrab->GetOwner()->GetActorLocation(),
	            ComponentToGrab->GetOwner()->GetActorRotation()
	            );

//		PhysicsHandle->GrabComponentAtLocation(
//				ComponentToGrab,
//				NAME_None,
//				ComponentToGrab->GetOwner()->GetActorLocation()
//		);

	}
}

void UGrabber::Release()
{
	UE_LOG(LogTemp, Warning, TEXT("Grab released!"));
	PhysicsHandle->ReleaseComponent();
}
